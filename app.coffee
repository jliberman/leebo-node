# Module dependencies.
express = require 'express'
path = require 'path'
TaskList = require './routes/tasklist.coffee'

# Create an express application.
exports.application = app = express()

app.set 'port', process.env.PORT || 3000
app.set 'views', __dirname + '/views'
app.set 'view engine', 'jade'
app.use express.favicon()
app.use express.logger('dev')
app.use express.bodyParser()
app.use express.methodOverride()
app.use app.router
app.use express.static(path.join(__dirname, 'public'))

dbConnection = process.env.CUSTOMCONNSTR_MONGOLAB_URI;

app.configure 'development', ->
  app.use express.errorHandler()
  dbConnection = 'localhost'

# Assign routes.
taskList = new TaskList dbConnection
app.get '/', taskList.showTasks
app.post '/addtask', taskList.addTask
app.post '/completetasks', taskList.completeTasks
