// Include the CoffeeScript interpreter so that .coffee files will work
var coffee = require('coffee-script');

// Include our application file
var app = require('./app.coffee');

// Start the server
app.application.listen(app.application.get('port'), function(){
  console.log('Express server listening on port ' + app.application.get('port'));
});