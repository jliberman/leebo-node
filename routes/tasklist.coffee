mongoose = require 'mongoose'
Task = require '../models/task.coffee'

class TaskList
	constructor: (conn) ->
		mongoose.connect(conn)

	# GET '/'
	showTasks: (req, res) =>
		Task.find(itemCompleted: false, (err, items) ->
			res.render 'index', {title: 'My ToDo List', tasks: items})

	# POST '/addtask'
	addTask: (req, res) =>
		@createTask req.body.item

		res.redirect '/'

	# POST '/completetasks'
	completeTasks: (req, res) =>
		@updateTask(id, done) for id, done of req.body.completed when done == 'true'

		res.redirect '/'

	createTask: (form) ->
		add = new Task()
		add.itemName = form.name
		add.itemCategory = form.category
		add.save @onError

	updateTask: (id, done) ->
		Task.update {_id: id}, {itemCompleted: done}, @onError

	onError: (err) ->
		throw err if err

module.exports = TaskList
